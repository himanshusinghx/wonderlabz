package com.xgileit.wonderlab.persitence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xgileit.wonderlab.persitence.domain.CustomerDetail;

public interface CustomerRepository extends JpaRepository<CustomerDetail, Long> {
	
	Optional<CustomerDetail> findByIdentificationNumber(String identificationNumber);

}
