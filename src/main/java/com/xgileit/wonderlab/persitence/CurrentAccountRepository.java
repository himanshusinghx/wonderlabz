package com.xgileit.wonderlab.persitence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xgileit.wonderlab.persitence.domain.CurrentAccount;

public interface CurrentAccountRepository extends JpaRepository<CurrentAccount, Long>{
	
	Optional<CurrentAccount> findByAccountNumber(Long accountNumber);

}
