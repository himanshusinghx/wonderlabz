package com.xgileit.wonderlab.persitence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xgileit.wonderlab.persitence.domain.SavingAccount;

public interface SavingAccountRepository extends JpaRepository<SavingAccount, Long>{
	
	Optional<SavingAccount> findByAccountNumber(Long accountNumber);

}
