package com.xgileit.wonderlab.persitence.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
@Entity
@DiscriminatorValue("CURRENT")
@JsonTypeName("CURRENT")
public class CurrentAccount extends BankAccount implements Serializable {

	private BigDecimal interestRate;
	
	@Column(nullable = false)
	private final BigDecimal overDraftLimit = new BigDecimal(100000.00d);
	
    private BigDecimal overDraftBalance;
    
    @Column(nullable = false)
    private BigDecimal balance;
}
