package com.xgileit.wonderlab.persitence.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.xgileit.wonderlab.enums.Gender;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
public class CustomerDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String title;

	@NotBlank
	private String firstName;

	@NotBlank
	private String lastName;

	//This could be any ID either Passport, National ID etc
	private String identificationNumber;

	private Gender gender;
	private Date dateOfBirth;
	private String maritalStatus;

	private String telephoneNumber;

	@Email
	private String email;

	private String streetAddress;
	private String streetAddressLine2;
	private String city;
	private String state;
	private String postal;
	private String country;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date lastUpdatedDate = new Date();

	@OneToMany(mappedBy = "accountHolder",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<BankAccount> accounts;
	

}
