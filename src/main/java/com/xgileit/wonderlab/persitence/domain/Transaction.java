package com.xgileit.wonderlab.persitence.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.xgileit.wonderlab.enums.TransactionStatus;
import com.xgileit.wonderlab.enums.TransactionType;

import lombok.Data;

@Data
@Entity
public class Transaction implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long tranactionId;

	@NotNull
	@Enumerated(EnumType.STRING)
	private TransactionType type;

	@NotNull
	@DecimalMin(value = "1.0", message = "Amount cannot be negetive")
	private BigDecimal amount;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TransactionStatus status=TransactionStatus.PENDING;

	private String createdBy;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private final Date transactionDate = new Date();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account", referencedColumnName = "account_id")
    private BankAccount account;

}
