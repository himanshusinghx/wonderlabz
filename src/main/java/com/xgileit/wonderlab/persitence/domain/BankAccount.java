package com.xgileit.wonderlab.persitence.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.xgileit.wonderlab.enums.AccountType;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.xgileit.wonderlab.enums.AccountStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="accountType",discriminatorType = DiscriminatorType.STRING,length = 7)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME,include = JsonTypeInfo.As.PROPERTY,property = "accountType")
@JsonSubTypes({
	@JsonSubTypes.Type(value=SavingAccount.class,name="SAVING"),
	@JsonSubTypes.Type(value=CurrentAccount.class,name="CURRENT")
})
public abstract class BankAccount implements Serializable {
	
	@Column(insertable = false,updatable = false, nullable = false)
	AccountType accountType;

	@Id
	@Column(name="account_id",updatable = false, nullable = false)
	protected Long accountId;

	@NotNull
	@Column(unique = true, updatable = false, nullable = false)
	protected Long accountNumber;

	@NotNull
	@Enumerated(EnumType.STRING)
	protected AccountStatus status = AccountStatus.ACTIVE;

	private String branchCode;
	private String address;
	
	@JsonBackReference
   	@ManyToOne
   	@JoinColumn(name = "user_id")
   	private CustomerDetail accountHolder;

	private String createdBy;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private final Date accountOpeningDate = new Date();

	private String updatedBy;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date lastUpdateDate = new Date();

}
