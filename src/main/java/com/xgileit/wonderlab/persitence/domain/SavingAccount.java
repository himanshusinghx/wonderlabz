package com.xgileit.wonderlab.persitence.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.DecimalMin;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@DiscriminatorValue("SAVING")
@JsonTypeName("SAVING")
@NoArgsConstructor
@Getter @Setter
public class SavingAccount extends BankAccount implements Serializable {
	
    @NotNull
    @DecimalMin(value = "1000.0", message = "Minimum balance should be 1000.0")
    private BigDecimal balance;
    

}
