package com.xgileit.wonderlab.persitence;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class Role implements GrantedAuthority {

    public static final String BANK_ADMIN = "BANK_ADMIN";
    public static final String BANK_USER = "BANK_USER";

    private String authority;

}