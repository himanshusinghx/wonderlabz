package com.xgileit.wonderlab.persitence;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xgileit.wonderlab.persitence.domain.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
