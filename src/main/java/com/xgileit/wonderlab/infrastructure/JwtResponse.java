package com.xgileit.wonderlab.infrastructure;

import java.io.Serializable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class JwtResponse implements Serializable {

	private final String jwttoken;
}