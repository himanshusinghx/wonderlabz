package com.xgileit.wonderlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WonderparkApplication {

	public static void main(String[] args) {
		SpringApplication.run(WonderparkApplication.class, args);
	}

}
