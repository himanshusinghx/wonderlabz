package com.xgileit.wonderlab.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import com.xgileit.wonderlab.persitence.domain.CustomerDetail;
import com.xgileit.wonderlab.rest.dto.*;
import com.xgileit.wonderlab.service.AccountService;
import com.xgileit.wonderlab.enums.TransactionType;
import com.xgileit.wonderlab.service.CustomerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import com.xgileit.wonderlab.persitence.Role;

import lombok.extern.slf4j.Slf4j;

@Tag(name = "Account")
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/wonderlab")
public class AccountController {

	private final AccountService accountService;
	private final CustomerService customerService;


	//@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@GetMapping(value = "/customer/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCustomerById(@PathVariable Long id) {
		return ResponseEntity.ok().body(customerService.getCustomerDetailById(id));
	}

	//@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@GetMapping(value = "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllCustomer() {
		return ResponseEntity.ok().body(customerService.getAllCustomer());
	}

	//@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@PostMapping(value = "/account", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> openNewAccount(@RequestBody AccountOpeningRequest request) {
		log.info("Request: {}", request);
		accountService.accountOpening(request);
		return ResponseEntity.ok("Success");
	}

	@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@PostMapping(value = "/transfer", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> moneyTransfer(@RequestBody MoneyTansferRequest request) {
		log.info("Request: {}", request);


		return ResponseEntity.ok("Success");
	}

	//@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@PostMapping(value = "/withdraw", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> withdraw(@RequestBody TransactionRequest request) {
		log.info("Request: {}", request);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		request.setTransactionType(TransactionType.WITHDRAW);
		accountService.withdraw(request);
		return ResponseEntity.ok("Success");
	}

	@RolesAllowed(Role.BANK_USER)
	@ResponseBody
	@PostMapping(value = "/deposit", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deposit(@RequestBody TransactionRequest request) {
		log.info("Request: {}", request);
		request.setTransactionType(TransactionType.DEPOSIT);
		accountService.deposit(request);
		return ResponseEntity.ok("Success");
	}

	@RolesAllowed(Role.BANK_USER)
	@RequestMapping("/statement/{accountNumber}")
	public ResponseEntity<?> getStatement(@PathVariable String accountNumber) {
		

		
		List<AccountStatement> statements = new ArrayList<>();
		return ResponseEntity.ok(statements);

	}

}
