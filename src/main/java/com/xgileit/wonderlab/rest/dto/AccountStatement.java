package com.xgileit.wonderlab.rest.dto;

import java.math.BigDecimal;
import java.util.List;

import com.xgileit.wonderlab.persitence.domain.Transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountStatement {
	
    private BigDecimal currentBalance;
    private List<Transaction> transactionHistory;

}
