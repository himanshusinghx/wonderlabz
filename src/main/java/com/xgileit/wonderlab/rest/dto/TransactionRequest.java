package com.xgileit.wonderlab.rest.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;
import com.xgileit.wonderlab.enums.AccountType;
import com.xgileit.wonderlab.enums.TransactionType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {

	@NotBlank
	private Long accountNumber;
	@NotBlank
	private AccountType accountType;

	private TransactionType transactionType;

	@NotNull
	@DecimalMin(value = "1.0", message = "Amount greater than 1")
	private BigDecimal amount;

}
