package com.xgileit.wonderlab.rest.dto;

import com.xgileit.wonderlab.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Setter @Getter
public class CustomerDetailRequest {


    private String title;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    //This could be any ID either Passport, National ID etc
    private String identificationNumber;
    private Gender gender;
    private Date dateOfBirth;
    private String maritalStatus;
    private String telephoneNumber;
    @Email
    private String email;
    private String streetAddress;
    private String streetAddressLine2;
    private String city;
    private String state;
    private String postal;
    private String country;
}
