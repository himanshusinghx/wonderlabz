package com.xgileit.wonderlab.rest.dto;

import com.sun.istack.NotNull;
import com.xgileit.wonderlab.enums.AccountType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@Setter @Getter
public class AccountOpeningRequest {

    @NotNull
    private String idNumber;

    @NotNull
    private AccountType type;

    @NotNull
    @DecimalMin(value = "1.0", message = "Amount greater than 1")
    private BigDecimal amount;

}
