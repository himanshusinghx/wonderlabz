package com.xgileit.wonderlab.rest.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTansferRequest {

	@NotBlank
	private String beneficiaryAccountNumber;

	@NotBlank
	private String beneficiaryFirstName;
	@NotBlank
	private String beneficiaryLastName;

	@NotNull
	@DecimalMin(value = "1.0", message = "Amount greater than 1")
	private BigDecimal amount;

}
