package com.xgileit.wonderlab.enums;

public enum TransactionType {
	
	WITHDRAW,
	DEPOSIT,
	MONEY_TRANSFER

}
