package com.xgileit.wonderlab.enums;

public enum Gender {

	MALE,
	FEMALE,
	UNKNOWN
}
