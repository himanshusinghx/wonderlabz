package com.xgileit.wonderlab.enums;

public enum TransactionStatus {
	
	COMPLETED,
	PENDING,
	FAILED

}
