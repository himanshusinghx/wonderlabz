package com.xgileit.wonderlab.enums;

public enum AccountStatus {

	PENDING, 
	ACTIVE, 
	DORMANT, 
	BLOCKED
}
