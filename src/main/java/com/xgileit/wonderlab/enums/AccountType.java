package com.xgileit.wonderlab.enums;

public enum AccountType {

    CURRENT,
    SAVING
}
