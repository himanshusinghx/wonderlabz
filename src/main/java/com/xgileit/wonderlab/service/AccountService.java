package com.xgileit.wonderlab.service;

import com.xgileit.wonderlab.enums.AccountStatus;
import com.xgileit.wonderlab.enums.AccountType;
import com.xgileit.wonderlab.persitence.CurrentAccountRepository;
import com.xgileit.wonderlab.persitence.CustomerRepository;
import com.xgileit.wonderlab.persitence.SavingAccountRepository;
import com.xgileit.wonderlab.persitence.domain.CurrentAccount;
import com.xgileit.wonderlab.persitence.domain.CustomerDetail;
import com.xgileit.wonderlab.persitence.domain.SavingAccount;
import com.xgileit.wonderlab.rest.dto.AccountOpeningRequest;
import com.xgileit.wonderlab.rest.dto.TransactionRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class AccountService {

    private final SavingAccountRepository savingAccountRepository;
    private final CurrentAccountRepository currentAccountRepository;
    private final CustomerRepository customerRepository;


    public void accountOpening(AccountOpeningRequest data){
        CustomerDetail customer = customerRepository.findByIdentificationNumber(data.getIdNumber())
                .orElseThrow(()-> new NotFoundException("Customer Detail Not Found"));
        if(data.getType() == AccountType.SAVING && data.getAmount().compareTo(BigDecimal.valueOf(1000))>=0){
            SavingAccount account = new SavingAccount();
            account.setStatus(AccountStatus.ACTIVE);
            account.setAccountNumber(getRandomLongNumber());
            account.setAccountHolder(customer);
            account.setBalance(data.getAmount());
            account.setAccountType(AccountType.SAVING);
            savingAccountRepository.save(account);
        }
        else if (data.getType() == AccountType.CURRENT){
                CurrentAccount account = new CurrentAccount();
                account.setStatus(AccountStatus.ACTIVE);
                account.setAccountNumber(getRandomLongNumber());
                account.setAccountHolder(customer);
                account.setBalance(data.getAmount());
                currentAccountRepository.save(account);
        }
    }

    public void deposit(TransactionRequest data){
        if(AccountType.CURRENT == data.getAccountType()){
            CurrentAccount account = currentAccountRepository.findByAccountNumber(data.getAccountNumber())
                    .orElseThrow(()->new RequestRejectedException("Account Number Invalid"));
            account.setBalance(account.getBalance().add(data.getAmount()));
            currentAccountRepository.save(account);
        }
        else if(AccountType.SAVING == data.getAccountType()){
            SavingAccount account = savingAccountRepository.findByAccountNumber(data.getAccountNumber())
                    .orElseThrow(()->new RequestRejectedException("Account Number Invalid"));
            account.setBalance(account.getBalance().add(data.getAmount()));
            savingAccountRepository.save(account);
        }
        else
            throw new RequestRejectedException("Invalid Account Type Provided");
    }

    public void withdraw(TransactionRequest data){
        if(AccountType.CURRENT == data.getAccountType()){
            CurrentAccount account = currentAccountRepository.findByAccountNumber(data.getAccountNumber())
                    .orElseThrow(()->new RequestRejectedException("Account Number Invalid"));
            BigDecimal newBalance = account.getBalance().add(account.getOverDraftBalance()).subtract(data.getAmount());
            if(validateBalance(newBalance,AccountType.CURRENT)){
                if(account.getBalance().compareTo(data.getAmount())>=0){
                    account.setBalance(account.getBalance().subtract(data.getAmount()));
                }
                else {
                    newBalance = data.getAmount().subtract(account.getBalance());
                    account.setBalance(BigDecimal.ZERO);
                    account.setOverDraftBalance(account.getOverDraftBalance().subtract(newBalance));
                    currentAccountRepository.save(account);
                }
            }
            else
                throw new RequestRejectedException("Insufficient Balance in Account");
        }
        else if(AccountType.SAVING == data.getAccountType()){
            SavingAccount account = savingAccountRepository.findByAccountNumber(data.getAccountNumber())
                    .orElseThrow(()->new RequestRejectedException("Account Number Invalid"));
            BigDecimal newBalance = account.getBalance().subtract(data.getAmount());
            if(validateBalance(newBalance,AccountType.SAVING)){
                account.setBalance(newBalance);
                savingAccountRepository.save(account);
            }
            else
                throw new RequestRejectedException("Insufficient Balance in Account");
        }
        else
            throw new RequestRejectedException("Invalid Account Type Provided");
    }

    private boolean validateBalance(BigDecimal current, AccountType type){
        if(AccountType.SAVING==type && current.compareTo(BigDecimal.valueOf(10000)) >= 0){
            return true;
        }
        else if(AccountType.CURRENT==type && current.compareTo(BigDecimal.valueOf(100000)) >= 0){
            return true;
        }
        else
            return false;
    }

    private Long getRandomLongNumber(){
        return ((long) (Math.random()*(10000000L - 1000000L))) + 10000000L;
    }

}
