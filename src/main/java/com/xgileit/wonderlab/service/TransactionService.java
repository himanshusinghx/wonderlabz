package com.xgileit.wonderlab.service;

import com.xgileit.wonderlab.enums.AccountType;
import com.xgileit.wonderlab.persitence.CurrentAccountRepository;
import com.xgileit.wonderlab.persitence.SavingAccountRepository;
import com.xgileit.wonderlab.persitence.TransactionRepository;
import com.xgileit.wonderlab.persitence.domain.Transaction;
import com.xgileit.wonderlab.rest.dto.TransactionRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository repository;
    private final SavingAccountRepository savingAccountRepository;
    private  final CurrentAccountRepository currentAccountRepository;

    public void saveTransaction(TransactionRequest data, String user, Objects account){
        Transaction transaction = new Transaction();
        transaction.setAmount(data.getAmount());
        transaction.setType(data.getTransactionType());
        transaction.setCreatedBy(user);
        if(data.getAccountType() == AccountType.SAVING){
           // transaction.setSavingAccount(savingAccountRepository.getById(data.getAccountNumber()));
        }
        if(data.getAccountType() == AccountType.CURRENT){
            //transaction.setCurrentAccount(currentAccountRepository.getById(data.getAccountNumber()));
        }
        //repository.save(transaction);
    }
}
