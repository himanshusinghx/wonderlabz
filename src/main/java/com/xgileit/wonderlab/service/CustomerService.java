package com.xgileit.wonderlab.service;

import com.xgileit.wonderlab.persitence.CustomerRepository;
import com.xgileit.wonderlab.persitence.domain.CustomerDetail;
import com.xgileit.wonderlab.rest.dto.CustomerDetailRequest;
import com.xgileit.wonderlab.utils.ObjectMapperUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerDetail addCustomer(CustomerDetailRequest request){
        CustomerDetail customer = customerRepository.findByIdentificationNumber(request.getIdentificationNumber())
                .orElse(new CustomerDetail());
        ObjectMapperUtils.map(request,customer);
        customer.setLastUpdatedDate(new Date());
        return customerRepository.save(ObjectMapperUtils.map(request,CustomerDetail.class));
    }

    public CustomerDetail getCustomerDetailById(Long id){
        return customerRepository.getById(id);
    }

    public List<CustomerDetail> getAllCustomer(){
        return customerRepository.findAll();
    }
}
