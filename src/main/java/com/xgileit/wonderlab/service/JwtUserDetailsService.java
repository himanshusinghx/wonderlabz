package com.xgileit.wonderlab.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.xgileit.wonderlab.persitence.Role;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (username.equals("wonderlab")) {
			List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new Role("BANK_USER"));
			return new User("wonderlab", "$2a$12$iouSdAYXPsoVV6xjjIa8Q.gMu5wtfuu1mHHoEtfimbNaH4yqGIdfO",authorities);
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}